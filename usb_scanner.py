import os
import time
import _thread
import psutil
from tkinter import *

v_l = ['exe', 'py', 'psi'] # virus list
usbs = [i.mountpoint for i in psutil.disk_partitions() if i.opts == 'rw,removable' and os.path.exists(i.mountpoint)]
s_u = '' # selected usb

w = Tk()
w.geometry("800x500")
w.title("USB Scanner")

usbFram = LabelFrame(w, text="USB Section")
usbFram.pack(side='left', padx=5, pady=5, fill="y", expand="yes")


filesFram = LabelFrame(w, text="Files Section")
filesFram.pack(side='right', padx=5, pady=5, fill="y", expand="yes")



def FUSB(e):
    # print(e.widget)
    # print(LBUSB.get(LBUSB.curselection()))
    global s_u
    s_u = LBUSB.curselection()[0]
    # print(v_l, 'no')
    LBFiles.delete(0, END)
    for path, subdirs, files in os.walk(os.path.join(LBUSB.get(LBUSB.curselection()[0]), 'fil')):
        for name in files:
            # print(os.path.join(path, name))
            # print(path, '\n', name)
            LBFiles.insert(END, os.path.join(path, name))
            LBFiles.itemconfigure("end", background='red' if name.split('.')[-1] in v_l else 'green')
# def FUSB(event):
#     print(event.widget.curselection())
    # event.widget.index(3)


LBUSB = Listbox(usbFram, selectmode=SINGLE, exportselection=False)
LBUSB.pack(padx=5, pady=50)
LBUSB.bind('<<ListboxSelect>>', FUSB)
for i in usbs:
    LBUSB.insert(END, i)





LBFiles = Listbox(filesFram, selectmode=MULTIPLE, width=80,
                  height=50, exportselection=False)

def FSA():
    # print('FSA')
    # print(LBFiles.curselection())
    # li = list(LBFiles.get(0,END))
    LBFiles.select_clear(0, "end")
    for i in range(0, LBFiles.size()):
        # print(i)
        # LBFiles.activate(i)
        LBFiles.selection_set(i)
        # LBFiles.see(i)
        # LBFiles.activate(i)
        # LBFiles.selection_anchor(i)

B = Button(usbFram, text ="Select All", command = FSA)

B.pack()



def FSI():
    # print('FSI')
    LBFiles.select_clear(0, "end")
    for i in range(0, LBFiles.size()):
        # print(i)
        # print(LBFiles.itemcget(i, "background"))
        if LBFiles.itemcget(i, "background") == 'red':
            LBFiles.selection_set(i)
        

B = Button(usbFram, text ="Select Infected", command = FSI)

B.pack()



def FUA():
    # print('FUA')
    LBFiles.select_clear(0, "end")

B = Button(usbFram, text ="Unselect All", command = FUA)

B.pack()


def FDS():
    # print('FDS')
    c = 0
    for i in LBFiles.curselection():
        # print(LBFiles.get(i-c))
        if os.path.exists(LBFiles.get(i-c)):
            os.remove(LBFiles.get(i-c))
            LBFiles.delete(i-c)
            c+=1

B = Button(usbFram, text ="Delete Selected", command = FDS)

B.pack()


LBFiles.pack(padx=5, pady=50)




def check_usb():
   while 1:
        time.sleep(1)
        new_usbs = [i.mountpoint for i in psutil.disk_partitions() if i.opts=='rw,removable' and os.path.exists(i.mountpoint)]
        if len(usbs) < len(new_usbs):
            for i in new_usbs:
                if not i in usbs:
                    LBUSB.insert(END, i)
                    usbs.append(i)
            print('Device add')
        elif len(usbs) > len(new_usbs):
            for i in usbs:
                if not i in new_usbs:
                    LBUSB.delete(usbs.index(i))
                    # print(f's_u : {s_u}')
                    # print(f'usbs.index(i) : {usbs.index(i)}')
                    if usbs.index(i) == s_u:
                        LBFiles.delete(0,END)
                    usbs.remove(i)
            print('Device removed')

        # print (usbs)


_thread.start_new_thread( check_usb, () )


w.mainloop()
